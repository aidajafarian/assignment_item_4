package crown_and_anchor_game;

import org.junit.Test;
import static org.junit.Assert.assertNotEquals;

/**
 * Bug 3: Odds in the game do not appear to be correct
 * Crown and Anchor games have an approximate 8% bias to the house.
 * So the win (win+lose) ratio should be approximately equal to 0.42.
 * This does not appear to be the case.
 * */
public class Bug3Test {

	
	/**
	 * The purpose of this test is to prove that the player can get
	 * any of the 6 possibilities of the dice within the same probability
	 * 
	 * Initial parameters:
	 * turns = 1250
	 * 
	 * Expected values:
	 * After 120 turns each value of the dice should appeared around 20 times
	 * but not 0 times.
	 * */
	@Test
	public void randomDiceValues() {
		
		// # counters
		int crownCounter = 0;
		int anchorCounter = 0;
		int hearCounter = 0;
		int diamondCounter = 0;
		int clubCounter = 0;
		int spadeCounter = 0;
		
		
		int turns = 120;
		Dice dice;
		
		// # simulating turns
		for(int i = 1; i <= turns; i++) {
			dice = new Dice();
			
			// # counting results
			switch(dice.getValue()) {
				case ANCHOR:
					anchorCounter++;
					break;
				case CLUB:
					clubCounter++;
					break;
				case CROWN:
					crownCounter++;
					break;
				case DIAMOND:
					diamondCounter++;
					break;
				case HEART:
					hearCounter++;
					break;
				case SPADE:
					spadeCounter++;
					break;
			}
		}
		
		// # printing results
		System.out.printf("Anchor results: %d\n", anchorCounter);
		System.out.printf("Club results: %d\n", clubCounter);
		System.out.printf("Crown results: %d\n", crownCounter);
		System.out.printf("Diamond results: %d\n", diamondCounter);
		System.out.printf("Heart results: %d\n", hearCounter);
		System.out.printf("Spade results: %d\n", spadeCounter);
		
		assertNotEquals(0, anchorCounter);
		assertNotEquals(0, clubCounter);
		assertNotEquals(0, crownCounter);
		assertNotEquals(0, diamondCounter);
		assertNotEquals(0, hearCounter);
		assertNotEquals(0, spadeCounter);
	}
}
