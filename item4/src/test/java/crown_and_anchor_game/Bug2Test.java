package crown_and_anchor_game;

import java.util.List;

import org.junit.Test;
import static org.junit.Assert.assertNotEquals;

/**
 * Bug 2: Player cannot reach betting limit
 * Limit set to 0, but game ends with player still with 5 (dollars) remaining
 * */
public class Bug2Test {


	/**
	 * The purpose of this test is to prove that the player can still betting while he has in his balance
	 * at least the same amount as his limit
	 * 
	 * Initial parameters:
	 * balance = 5
	 * bet = 5
	 * limit = 0
	 * 
	 * Expected final balance: Amount different of 5
	 * Which means the player even lost his final 5 or he has won. Either way he was able to bet one more time 
	 * */
	@Test
	public void reachBettingLimit() {
		
		final int bet = 5;
		
		// # setting up dices
		Dice d1 = new Dice();
        Dice d2 = new Dice();
        Dice d3 = new Dice();
        
		// # setting up a Player with balance = 5
		Player player = new Player("Fred", 5);
		
		// # setting up a game
		Game game = new Game(d1, d2, d3);
		List<DiceValue> cdv = game.getDiceValues();	
		
		int winnings = player.getBalance();
		if (player.balanceExceedsLimitBy(bet)) {
			
			DiceValue pick = DiceValue.getRandom();
			
			System.out.printf("Turn %d: %s bet %d on %s\n",
        			1, player.getName(), bet, pick); 
			
			winnings = game.playRound(player, pick, bet);
            cdv = game.getDiceValues();
            
            System.out.printf("Rolled %s, %s, %s\n",
            		cdv.get(0), cdv.get(1), cdv.get(2));
            
            if (winnings > 0) {
                System.out.printf("%s won %d, balance now %d\n\n", player.getName(), winnings, player.getBalance());
            }
            else {
                System.out.printf("%s lost, balance now %d\n\n", player.getName(), player.getBalance());
            }
		}
		
		assertNotEquals(5, player.getBalance());
	}
}
