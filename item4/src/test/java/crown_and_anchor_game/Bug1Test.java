package crown_and_anchor_game;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;


/**
 * Bug 1: Game doesn�t pay out a correct level
 * When player wins on 1 match, balance doesn�t increase
 * 
 * */
public class Bug1Test {

	/**
	 * The purpose of this test is to prove that the player is paid properly when He wins on 1 match
	 * 
	 * Initial parameters:
	 * balance = 100
	 * bet = 5
	 * limit = 0
	 * 
	 * Expected final balance: 105
	 * 
	 * In order to complete with this test, all dices must have different values.
	 * 
	 * */
	@Test
	public void payOutACorrectLevel() {

		int bet = 5;
		
		// # setting up dices
		Dice d1 = new Dice();
        Dice d2 = new Dice();
        
        while(d1.getValue().equals(d2.getValue())) {
        	d2 = new Dice(); // making sure we get 3 different values
        }
        
        Dice d3 = new Dice();
        while(d1.getValue().equals(d3.getValue()) || d2.getValue().equals(d3.getValue())) {
        	d3 = new Dice(); // making sure we get 3 different values
        }
		
		// # setting up a Player with balance = 100
		Player player = new Player("Fred", 100);
		
		// # setting up a game
		Game game = new Game(d1, d2, d3);
		List<DiceValue> cdv = game.getDiceValues();	

		// for testing purpose players pick will always be equals to d1 value
		DiceValue pick = d1.getValue();
		
		System.out.printf("Turn %d: %s bet %d on %s\n",
    			1, player.getName(), bet, pick); 
		
		int winnings = game.playRound(player, pick, bet);
		
		System.out.printf("Rolled %s, %s, %s\n",
        		cdv.get(0), cdv.get(1), cdv.get(2));
        
        if (winnings > 0) {
            System.out.printf("%s won %d, balance now %d\n\n", player.getName(), winnings, player.getBalance());
        } else {
            System.out.printf("%s lost, balance now %d\n\n", player.getName(), player.getBalance());
        }

        System.out.println(String.format("%s now has balance %d\n", player.getName(), player.getBalance()));
        
        assertEquals(105, player.getBalance());
	}
}
